﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameMechanics : MonoBehaviour
{
	[SerializeField] private DoubleSpawner _doubleSpawner;
	[SerializeField] private UI _ui;
	[SerializeField] private Text _timerText;
	
	[SerializeField] private int _timeChangeDifficulty;
	[SerializeField] private bool _changedDifficulty;

	public float BuffSecondLifeActionTime;
	public bool GameOver = true;
	public bool OpenMenu = true;

	private float _timer = 0f;
	private int _minutes = 0;

	private void Update()
	{
		if (!GameOver && !OpenMenu)
			Timer();
	}

	private void Timer()
	{
		if (Math.Round(_timer) == 60)
		{
			_timer = 0;
			_minutes++;
		}
		_timer += Time.deltaTime;
		_timerText.text = _minutes + ":" + Math.Round(_timer);

		if(!_changedDifficulty)
			if (Math.Round(_timer) == _timeChangeDifficulty || Math.Round(_timer) == 2 * _timeChangeDifficulty)
				ChangeTheDifficulty();
	}

	private void ChangeTheDifficulty()
	{
		_changedDifficulty = true;
		Invoke(nameof(ChangeStateDifficulty), 5f);
		if (_doubleSpawner._timeDelay > 0.4f)
			_doubleSpawner._timeDelay -= 0.2f;
	}

	public void GameOverEnable()
	{
		Time.timeScale = 0f;
		GameOver = true;
		_ui.OpenLoseWindow();
	}

	public void ResumeGame() 
	{
		Time.timeScale = 1f;
		GameOver = false;
	}

	public void StartGame()
	{
		Time.timeScale = 1f;
		GameOver = false;
		OpenMenu = false;
	}

	private void ChangeStateDifficulty() => _changedDifficulty = false;
}