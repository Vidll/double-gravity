﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
	[SerializeField] private GameMechanics _gameMechanics;
	[SerializeField] private DontDestroyClass _dontDestroyClass;
	[SerializeField] private GameObject _startWindow;
	[SerializeField] private GameObject _loseWindow;

	private bool _gameOver = false;
	private void Start()
	{
		_dontDestroyClass = GameObject.FindGameObjectWithTag("DontDestoy").GetComponent<DontDestroyClass>();
		_dontDestroyClass.DestoyThis = false;
		CloseAllWindows();
		
		if (_dontDestroyClass.FirstGame)
			OpenMenu();
		else
			StartGameButton();
	}

	public void StartGameButton()
	{
		_gameMechanics.StartGame();
		SetDontFirstGame();
		_startWindow.SetActive(false);
	}

	public void OpenMenu()
	{
		if (_gameOver)
			return;

		CloseAllWindows();
		Time.timeScale = 0f;
		_gameMechanics.OpenMenu = true;
		_startWindow.SetActive(true);
	}

	private void CloseAllWindows()
	{
		_startWindow.SetActive(false);
		_loseWindow.SetActive(false);
	}

	public void OpenLoseWindow()
	{
		CloseAllWindows();
		_gameOver = true;
		_loseWindow.SetActive(true);
	}

	public void OnClickViewVideo()
	{
		ResumeGame();
	}

	private void ResumeGame()
	{
		_gameOver = false;
		_gameMechanics.ResumeGame();
		_loseWindow.SetActive(false);
	}

	public void RestartButton() => SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	private void SetDontFirstGame() => _dontDestroyClass.FirstGame = false;
	public void ExitButton() => Application.Quit();
}
