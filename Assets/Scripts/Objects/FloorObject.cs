﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorObject : ObjectsYPositions
{
	private void Start()
	{
		var posY = Mathf.Round(transform.position.y);
		if (posY == Mathf.Round(_ySpawnPositionDown[0]) || posY == Mathf.Round(_ySpawnPositionUp[0]))
			transform.Rotate(Vector3.right * 180);
	}
}
