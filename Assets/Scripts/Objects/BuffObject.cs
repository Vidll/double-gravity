﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffObject : ObjectsYPositions
{
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Player"))
		{
			var player = collision.gameObject.GetComponent<PlayerController>();
			player.CancelDisebleSecondLife();
			player.StartDisableBuffSecondLife();
			Destroy(this.gameObject);
		}
	}
}
