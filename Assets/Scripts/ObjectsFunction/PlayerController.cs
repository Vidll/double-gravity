﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	[SerializeField] private Button _buttonController;
	[SerializeField] private GameMechanics _gameMechanics;
	[SerializeField] private GameObject _fluBuffEffect;

	[SerializeField] private float _gravityValue;
	[SerializeField] private bool OnFloor = true;

	private Rigidbody2D _rigidbody2D;

	private bool BuffSecondLife;

	private void Start()
	{
		_rigidbody2D = GetComponent<Rigidbody2D>();
		if (_buttonController != null)
			_buttonController.onClick.AddListener(ClickOnButton);
	}

	private void ClickOnButton()
	{
		if (!_gameMechanics.GameOver)
		{
			if (OnFloor)
				ChangeGravitation(-_gravityValue, -180);
			else
				ChangeGravitation(_gravityValue, 180);
			OnFloor = !OnFloor;
		}
	}

	private void ChangeGravitation(float value, float angle)
	{
		this.gameObject.transform.Rotate(Vector3.right * angle);
		_rigidbody2D.gravityScale = value;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Obstacle"))
		{
			if (BuffSecondLife)
			{
				Destroy(collision.gameObject);
				CancelDisebleSecondLife();
				DisableSecondLife();
				return;
			}
			_gameMechanics.GameOverEnable();
		}
	}

	public void StartDisableBuffSecondLife() 
	{
		BuffSecondLife = true;
		_fluBuffEffect.SetActive(true);
		Invoke(nameof(DisableSecondLife), _gameMechanics.BuffSecondLifeActionTime);
	}
	
	private void DisableSecondLife()
	{
		BuffSecondLife = false;
		_fluBuffEffect.SetActive(false);
	}

	public void CancelDisebleSecondLife() => CancelInvoke(nameof(DisableSecondLife));
}
