﻿using UnityEngine;

public abstract class ObjectsYPositions : MonoBehaviour
{
    public float[] _ySpawnPositionUp;
    public float[] _ySpawnPositionDown;
}
