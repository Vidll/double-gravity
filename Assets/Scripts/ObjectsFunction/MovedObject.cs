﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovedObject : MonoBehaviour , IMoved
{
	[SerializeField] private float _moveSpeed = 5f;
	[SerializeField] private float _xPosDestroy = -13;

	private void Update()
	{
		Moved(this.gameObject.transform,_moveSpeed);

		if (this.transform.position.x < _xPosDestroy)
			DestroyGameObject(this.gameObject);
	}

	private void DestroyGameObject(GameObject obj) => Destroy(obj);
	public void Moved(Transform gameObjectTransform, float moveSpeed) => gameObjectTransform.Translate(Vector3.left *  moveSpeed * Time.deltaTime);
}
