﻿using UnityEngine;

public class MovedBgAndFloor : MonoBehaviour
{
	[SerializeField] private float _moveSpeed = 5f;
	[SerializeField] private float _destroyXPosition;
	[SerializeField] private Vector3 _startPosition;

	private void Update()
	{
		Moved();
		if (this.gameObject.transform.position.x <= _destroyXPosition)
			this.gameObject.transform.position = _startPosition;
	}

	private void Moved() => this.gameObject.transform.Translate(Vector3.left * _moveSpeed * Time.deltaTime);
}
