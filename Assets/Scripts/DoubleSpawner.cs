﻿using UnityEngine;

public class DoubleSpawner : MonoBehaviour
{
	[SerializeField] private GameObject[] _floorObjects;
	[SerializeField] private GameObject[] _flyObjects;
	[SerializeField] private GameObject[] _buffers;

	[SerializeField] private float _xSpawnPosition = 10f;
	public float _timeDelay;
	
	private bool UpOrDown;

	private void Start()
	{
		Invoke(nameof(CreateObstacle), _timeDelay);
	}

	public void CreateObstacle()
	{
		ChooseAndCreateObstacle();
		Invoke(nameof(ChooseAndCreateObstacle), GetFloatRandomNumber(0.5f, 2f));
		Invoke(nameof(CreateObstacle), _timeDelay);
	}

	private void ChooseAndCreateObstacle()
	{
		UpOrDown = !UpOrDown;
		var randomObject = GetIntRandomNumber(1, 10);
		switch (randomObject)
		{
			case 1:
				var objectPositionsClass = _floorObjects[GetIntRandomNumber(0, _floorObjects.Length)].GetComponent<ObjectsYPositions>();
				Create(objectPositionsClass.gameObject, new Vector2(_xSpawnPosition, UpOrDown ?
					objectPositionsClass._ySpawnPositionUp[GetIntRandomNumber(0, objectPositionsClass._ySpawnPositionUp.Length)] :
					objectPositionsClass._ySpawnPositionDown[GetIntRandomNumber(0, objectPositionsClass._ySpawnPositionDown.Length)]));
				break;
			case 2:
				objectPositionsClass = _flyObjects[GetIntRandomNumber(0, _flyObjects.Length)].GetComponent<ObjectsYPositions>();
				Create(objectPositionsClass.gameObject, new Vector2(_xSpawnPosition, UpOrDown ?
					GetFloatRandomNumber(objectPositionsClass._ySpawnPositionUp[0], objectPositionsClass._ySpawnPositionUp[1]) :
					GetFloatRandomNumber(objectPositionsClass._ySpawnPositionDown[0], objectPositionsClass._ySpawnPositionDown[1])));
				break;
			case 9:
				CreateBuff();
				break;
			default:
				goto case 1;
		}
	}

	private void Create(GameObject gameobject, Vector2 pos)
	{
		var obj = Instantiate(gameobject);
		obj.transform.position = pos;
		obj.transform.parent = this.gameObject.transform;
	}

	public void CreateBuff()
	{
		var objectPositionsClass = _buffers[GetIntRandomNumber(0, _buffers.Length)].GetComponent<ObjectsYPositions>();
		Create(objectPositionsClass.gameObject, new Vector2(_xSpawnPosition, UpOrDown ?
			GetFloatRandomNumber(objectPositionsClass._ySpawnPositionUp[0], objectPositionsClass._ySpawnPositionUp[1]) :
			GetFloatRandomNumber(objectPositionsClass._ySpawnPositionDown[0], objectPositionsClass._ySpawnPositionDown[1])));
	}

	private int GetIntRandomNumber(int rangeA, int rangeB) => Random.Range(rangeA, rangeB);
	private float GetFloatRandomNumber(float rangeA, float rangeB) => Random.Range(rangeA, rangeB);
}