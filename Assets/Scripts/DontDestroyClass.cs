﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyClass : MonoBehaviour
{
	public bool FirstGame = true;
	public bool DestoyThis = true;

	private void Start()
	{
		DontDestroyOnLoad(this.gameObject);
		Invoke(nameof(CheckDuplicateAndDestoy), 2f);
	}

	private void CheckDuplicateAndDestoy()
	{
		if (DestoyThis)
			Destroy(this.gameObject);
	}
}
