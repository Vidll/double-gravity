﻿using UnityEngine;

interface IMoved 
{
    void Moved(Transform gameObjectTransform, float moveSpeed);
}
